#ifndef PARSE_H
#define PARSE_H

#include "light.h"

struct light_conf *parse_args(int argc, char **argv);

#endif /* PARSE_H */
