#ifndef INIT_H
#define INIT_H

#include <stdbool.h>

bool init_strings(struct light_conf *conf);

#endif /* INIT_H */
